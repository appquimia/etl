﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Data;

namespace tw_appquimia
{
    class Program
    {
        static void Main(string[] args)
        {
            var logger = new Logger();

            string mssql_cnstr = "Data Source=190.228.83.52;Initial Catalog=GestionAdministrativa.Database;User ID=db_viewer;Password=DBViewer#10";
            SqlConnection mssql_cnn = null;// = new SqlConnection(mssql_cnstr);

            //string mysql_cnstr = "SERVER=192.168.1.24;DATABASE=appquimia;UID=guest;PASSWORD=guest123;";
            string mysql_cnstr = "SERVER=179.43.121.9;DATABASE=c0940151_appdb;UID=guest;PASSWORD=guest123;";
            MySqlConnection mysql_conn = null; //new MySqlConnection(mysql_cnstr);
            MySqlTransaction mytr = null;

            try
            {
                var dsMoviles = new DataSet();
                var dsConductores = new DataSet();
                var dsResponsables = new DataSet();
                var dsConductoresPorMovil = new DataSet();

                using (mssql_cnn = new SqlConnection(mssql_cnstr))
                {

                    mssql_cnn.Open();
                    logger.Info("Conexión MS SQL abierta ", true);

                    dsMoviles = GetDataset("SELECT * FROM MovilesVista", mssql_cnn);
                    logger.Info("MS SQL Moviles obtenidos", true);

                    dsConductores = GetDataset("SELECT * FROM Conductores", mssql_cnn);
                    logger.Info("MS SQL Conductores obtenidos", true);

                    dsResponsables = GetDataset("SELECT * FROM Responsables", mssql_cnn);
                    logger.Info("MS SQL Responsables obtenidos", true);

                    dsConductoresPorMovil = GetDataset("SELECT * FROM ConductoresPorMovil", mssql_cnn);
                    logger.Info("MS SQL ConductoresPorMoviles obtenidos", true);
                    mssql_cnn.Close();
                }

                using (mysql_conn = new MySqlConnection(mysql_cnstr))
                {

                    mysql_conn.Open();
                    logger.Info("Conexión MySQL abierta ", true);

                    mytr = mysql_conn.BeginTransaction();

                    ABMMoviles(dsMoviles.Tables[0], logger, mysql_conn, mytr);

                    ABMConductores(dsConductores.Tables[0], logger, mysql_conn, mytr);

                    ABMResponsables(dsResponsables.Tables[0], logger, mysql_conn, mytr);

                    ABMConductoresXMoviles(dsConductoresPorMovil.Tables[0], logger, mysql_conn, mytr);

                    mytr.Commit();
                    mysql_conn.Close();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                try
                {
                    if (mytr != null)
                        mytr.Rollback();
                }
                catch (SqlException ex1)
                {
                    if (mytr.Connection != null)
                    {
                        logger.Error(ex1.Message);
                    }
                }
            }
            finally
            {
                if(mssql_cnn != null)
                    mssql_cnn.Close();

                if (mysql_conn != null)
                    mysql_conn.Close();
            }
            Console.WriteLine("Presione enter para finalizar...");
            Console.ReadLine();
        }

        static DataSet GetDataset(string sQuery, SqlConnection conn)
        {
            DataSet ds = new DataSet();
            var command = new SqlCommand(sQuery, conn);
            var adapter = new SqlDataAdapter(command);
            adapter.Fill(ds);

            return ds;
        }

        static void ABMMoviles(DataTable dt, Logger logger, MySqlConnection mysql_conn, MySqlTransaction mytr)
        {
            var mysql_cmd = new MySqlCommand("CREATE TEMPORARY TABLE tmp_moviles (id VARCHAR(36), numero INT, descripcion VARCHAR(50), dominio VARCHAR(10), responsable VARCHAR(36));", mysql_conn, mytr);
            mysql_cmd.ExecuteNonQuery();
            logger.Info("Tabla tmp_moviles creada ", true);
                        
            MySqlCommand comm;

            foreach (DataRow dr in dt.Rows)
            {
                comm = mysql_conn.CreateCommand();
                comm.CommandText = "INSERT INTO tmp_moviles(id, numero, descripcion, dominio, responsable) VALUES(?id, ?numero, ?descripcion, ?dominio, ?responsable)";
                comm.Parameters.AddWithValue("?id", dr["codigo"].ToString());
                comm.Parameters.AddWithValue("?numero", dr["numero"].ToString());
                comm.Parameters.AddWithValue("?descripcion", dr["descripcion"].ToString());
                comm.Parameters.AddWithValue("?dominio", dr["dominio"].ToString());
                comm.Parameters.AddWithValue("?responsable", dr["responsable"].ToString());
                comm.ExecuteNonQuery();
            }
            logger.Info("Tabla tmp_moviles cargada ", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "INSERT INTO moviles (id, nromovil, descripcion, dominio, idresponsable, activo) " +
                "SELECT tm.id, tm.numero, tm.descripcion, tm.dominio, tm.responsable, 1 " +
                "FROM tmp_moviles tm " +
                "WHERE tm.id NOT IN (SELECT id FROM moviles);";
            comm.ExecuteNonQuery();
            logger.Info("Nuevos móviles cargados (Abm)", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "UPDATE moviles m " +
                "INNER JOIN tmp_moviles tm ON m.id = tm.id " +
                "SET m.id = tm.id, m.nromovil = tm.numero, m.descripcion = tm.descripcion, m.dominio = tm.dominio, m.idresponsable = tm.responsable, m.activo = 1;";
            comm.ExecuteNonQuery();
            logger.Info("Móviles actualizados (abM)", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "UPDATE moviles m " +
                "SET m.activo = 0 " +
                "WHERE m.id NOT IN (SELECT tm.id FROM tmp_moviles tm);";
            comm.ExecuteNonQuery();
            logger.Info("Móviles no activos actualizados (aBm)", true);
        }

        static void ABMConductores(DataTable dt, Logger logger, MySqlConnection mysql_conn, MySqlTransaction mytr)
        {

            var mysql_cmd = new MySqlCommand("CREATE TEMPORARY TABLE tmp_conductores (id VARCHAR(36), apellido VARCHAR(100), nombre VARCHAR(100), dni INT);", mysql_conn, mytr);
            mysql_cmd.ExecuteNonQuery();
            logger.Info("Tabla tmp_conductores creada ", true);

            MySqlCommand comm;

            foreach (DataRow dr in dt.Rows)
            {
                comm = mysql_conn.CreateCommand();
                comm.CommandText = "INSERT INTO tmp_conductores(id, dni, apellido, nombre) VALUES(?id, ?dni, ?apellido, ?nombre)";
                comm.Parameters.AddWithValue("?id", dr["id"].ToString());
                comm.Parameters.AddWithValue("?dni", dr["dni"].ToString());
                comm.Parameters.AddWithValue("?apellido", dr["apellido"].ToString());
                comm.Parameters.AddWithValue("?nombre", dr["nombre"].ToString());
                comm.ExecuteNonQuery();
            }
            logger.Info("Tabla tmp_conductores cargada ", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "INSERT INTO conductores (id, apellidos, nombres, dni, activo) " +
                "SELECT tc.id, tc.apellido, tc.nombre, tc.dni, 1 " +
                "FROM tmp_conductores tc " +
                "WHERE tc.id NOT IN (SELECT id FROM conductores);";
            comm.ExecuteNonQuery();
            logger.Info("Nuevos conductores cargados (Abm)", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "UPDATE conductores c " +
                "INNER JOIN tmp_conductores tc ON c.id = tc.id " +
                "SET c.id = tc.id, c.apellidos = tc.apellido, c.nombres = tc.nombre, c.dni = tc.dni, c.activo = 1;";
            comm.ExecuteNonQuery();
            logger.Info("Conductores actualizados (abM)", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "UPDATE conductores c " +
                "SET c.activo = 0 " +
                "WHERE c.id NOT IN (SELECT tc.id FROM tmp_conductores tc)";
            comm.ExecuteNonQuery();
            logger.Info("Conductores no activos actualizados (aBm)", true);
        }

        static void ABMResponsables(DataTable dt, Logger logger, MySqlConnection mysql_conn, MySqlTransaction mytr)
        {
            var mysql_cmd = new MySqlCommand("CREATE TEMPORARY TABLE tmp_responsables (id VARCHAR(36), apellidos VARCHAR(100), nombres VARCHAR(100), dni INT, telefono VARCHAR(50));", mysql_conn, mytr);
            mysql_cmd.ExecuteNonQuery();
            logger.Info("Tabla tmp_responsables creada ", true);

            MySqlCommand comm;

            foreach (DataRow dr in dt.Rows)
            {
                comm = mysql_conn.CreateCommand();
                comm.CommandText = "INSERT INTO tmp_responsables(id, apellidos, nombres, dni, telefono) VALUES(?id, ?apellidos, ?nombres, ?dni, ?telefono)";
                comm.Parameters.AddWithValue("?id", dr["codigo"].ToString());
                comm.Parameters.AddWithValue("?apellidos", dr["apellidos"].ToString());
                comm.Parameters.AddWithValue("?nombres", dr["nombres"].ToString());
                comm.Parameters.AddWithValue("?dni", dr["dni"].ToString());
                comm.Parameters.AddWithValue("?telefono", dr["telefono"].ToString());
                comm.ExecuteNonQuery();
            }
            logger.Info("Tabla tmp_responsables cargada ", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "INSERT INTO responsables (id, apellidos, nombres, dni, telefono, activo) " +
                "SELECT tc.id, tc.apellidos, tc.nombres, tc.dni, tc.telefono, 1 " +
                "FROM tmp_responsables tc " +
                "WHERE tc.id NOT IN (SELECT id FROM responsables);";
            comm.ExecuteNonQuery();
            logger.Info("Nuevos responsables cargados (Abm)", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "UPDATE responsables c " +
                "INNER JOIN tmp_responsables tc ON c.id = tc.id " +
                "SET c.id = tc.id, c.apellidos = tc.apellidos, c.nombres = tc.nombres, c.dni = tc.dni, c.telefono = tc.telefono, c.activo = 1;";
            comm.ExecuteNonQuery();
            logger.Info("Responsables actualizados (abM)", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "UPDATE responsables c " +
                "SET c.activo = 0 " +
                "WHERE c.id NOT IN (SELECT tc.id FROM tmp_responsables tc)";
            comm.ExecuteNonQuery();
            logger.Info("Responsables no activos actualizados (aBm)", true);
        }

        static void ABMConductoresXMoviles(DataTable dt, Logger logger, MySqlConnection mysql_conn, MySqlTransaction mytr)
        {
            var mysql_cmd = new MySqlCommand("CREATE TEMPORARY TABLE tmp_conductoresxmoviles (id INT, chofercodigo VARCHAR(36), movilcodigo VARCHAR(36));", mysql_conn, mytr);
            mysql_cmd.ExecuteNonQuery();
            logger.Info("Tabla tmp_conductoresxmoviles creada ", true);

            MySqlCommand comm;

            foreach (DataRow dr in dt.Rows)
            {
                comm = mysql_conn.CreateCommand();
                comm.CommandText = "INSERT INTO tmp_conductoresxmoviles(id, chofercodigo, movilcodigo) VALUES(?id, ?chofercodigo, ?movilcodigo)";
                comm.Parameters.AddWithValue("?id", dr["codigo"].ToString());
                comm.Parameters.AddWithValue("?chofercodigo", dr["chofercodigo"].ToString());
                comm.Parameters.AddWithValue("?movilcodigo", dr["movilcodigo"].ToString());
                comm.ExecuteNonQuery();
            }
            logger.Info("Tabla tmp_conductoresxmoviles cargada ", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "INSERT INTO conductoresxmoviles (id, idconductor, idmovil, activo) " +
                "SELECT tc.id, tc.chofercodigo, tc.movilcodigo, 1 " +
                "FROM tmp_conductoresxmoviles tc " +
                "WHERE tc.id NOT IN (SELECT id FROM conductoresxmoviles);";
            comm.ExecuteNonQuery();
            logger.Info("Nuevos conductoresxmoviles cargados (Abm)", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "UPDATE conductoresxmoviles c " +
                "INNER JOIN tmp_conductoresxmoviles tc ON c.id = tc.id " +
                "SET c.id = tc.id, c.idconductor = tc.chofercodigo, c.idmovil = tc.movilcodigo, c.activo = 1;";
            comm.ExecuteNonQuery();
            logger.Info("Conductoresxmoviles actualizados (abM)", true);

            comm = mysql_conn.CreateCommand();
            comm.CommandText = "UPDATE conductoresxmoviles c " +
                "SET c.activo = 0 " +
                "WHERE c.id NOT IN (SELECT tc.id FROM tmp_conductoresxmoviles tc)";
            comm.ExecuteNonQuery();
            logger.Info("Conductoresxmoviles no activos actualizados (aBm)", true);
        }
    }
}
